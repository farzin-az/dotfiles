#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    "$@"&
  fi
}

run nm-applet
run picom
run element-desktop --hidden
run clipit
run udiskie
run light-locker
run /home/farzin/Code/scripts/feh.sh
run xidlehook --not-when-audio --timer 900 "systemctl suspend" ""
