#!/usr/bin/zsh

alias v="nvim"
alias muse="ncmpcpp"
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME' 
alias unmount="udisksctl unmount -b"
alias pl="playlist_maker.sh"

alias files="ranger"

