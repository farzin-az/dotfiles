local opts = { noremap = true }
local keymap = vim.keymap.set
vim.g.mapleader = ' '
keymap('n', '<leader>l', '<Cmd>nohls<CR>', opts)
keymap('n', '<leader>p', '<Cmd>set keymap=persian<CR>', opts)
keymap('n', '<leader>e', '<Cmd>set keymap=<CR>', opts)
keymap('n', '<leader>x', '<Cmd>bd<CR>', opts)
keymap('n', '<Shift>l', '<Cmd>bn<CR>', opts)
keymap('n', '<Shift>h', '<Cmd>bp<CR>', opts)
keymap('n', '<leader>s', '<Cmd>source %<CR>', opts)
keymap('n', '<leader><Space>', '<Cmd>Telescope commands<CR>', opts)
keymap('n', '<leader>ee', '<Cmd>q<CR>', opts)
keymap('n', '<leader>f', '<Cmd>lua vim.lsp.buf.formatting_sync()<CR>', opts)
