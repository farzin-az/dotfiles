return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	-- basic stuff
	use 'lambdalisue/suda.vim'
	use 'https://github.com/tpope/vim-commentary'
	use 'Pocco81/auto-save.nvim'
	use 'tpope/vim-surround'
	use 'jiangmiao/auto-pairs'

	-- colorscheme
	use 'navarasu/onedark.nvim'

	-- statusline
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}

	-- Treesitter
	use {
		'nvim-treesitter/nvim-treesitter',
		run = function() require('nvim-treesitter.install').update({ with_sync = true }) end,
	}
	use 'p00f/nvim-ts-rainbow'

	-- fuzzy finder
	use {
		'nvim-telescope/telescope.nvim', tag = '0.1.0',
		requires = { { 'nvim-lua/plenary.nvim' } }
	}

	-- lsp
	use 'williamboman/mason.nvim'
	use 'williamboman/mason-lspconfig.nvim'
	use 'neovim/nvim-lspconfig'

	-- cmp plugins
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-cmdline'
	use 'hrsh7th/cmp-nvim-lua'
	use 'hrsh7th/nvim-cmp'

	-- snippets
	use 'L3MON4D3/LuaSnip'
	use 'saadparwaiz1/cmp_luasnip'

	-- git
	use {
		'lewis6991/gitsigns.nvim',
		config = function()
			require('gitsigns').setup()
		end
	}

	-------------- filetypes ----------------
	-- markdown
	use 'godlygeek/tabular'
	use { 'preservim/vim-markdown',
		config = function()
			vim.g.vim_markdown_math = 1
		end }

	-- latex
	use 'lervag/vimtex'
end)
