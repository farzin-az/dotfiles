local options = {
		encoding = 'utf-8',
		number = true,
		swapfile = false,
		tabstop = 4,
		autoindent = true,
		mouse = 'a',
		ignorecase = true,
		termbidi = true,
		splitbelow = true,
		syntax = 'enable',
		clipboard = 'unnamedplus'
}

for key, value in pairs(options) do
		vim.opt[key] = value
end

vim.cmd [[set iskeyword+=-]]
