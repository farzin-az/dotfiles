local handlers = require('lsp/handlers')
local mason_lspconfig = require('mason-lspconfig')
local lspconfig = require('lspconfig')

local servers = { "lua_ls", "jedi_language_server", "bashls" }
mason_lspconfig.setup({
	ensure_installed = servers
})

for _, server in pairs(servers) do
	local opts = {
		on_attach = handlers.on_attach,
		capabilities = handlers.capabilities,
	}
	local has_custom_opts, server_custom_opts = pcall(require, "lsp.settings." .. server)
	if has_custom_opts then
		opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
	end
	lspconfig[server].setup(opts)
end
