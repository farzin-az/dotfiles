return {
	settings = {

		Lua = {
			diagnostics = {
				globals = { "vim", "client", "awesome", "mouse", "screen", "root"},
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.stdpath("config") .. "/lua"] = true,
				},
			},
		},
	},
}
