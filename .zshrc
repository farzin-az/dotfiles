source $HOME/.config/zsh/antigen.zsh
antigen theme romkatv/powerlevel10k
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle kutsan/zsh-system-clipboard
antigen apply

source $HOME/.config/zsh/p10k.zsh
source $HOME/.config/zsh/aliases.zsh

# options
setopt autocd
# unsetopt menu_complete
# unsetopt flowcontrol
# setopt prompt_subst
# setopt always_to_end
setopt append_history
# setopt auto_menu
# setopt complete_in_word
setopt extended_history
# setopt hist_expire_dups_first
# setopt hist_ignore_dups
# setopt hist_ignore_space
# setopt hist_verify
# setopt inc_append_history
setopt share_history

## case-insensitive (uppercase from lowercase) completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
## case-insensitive (all) completion
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
## case-insensitive,partial-word and then substring completion
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
