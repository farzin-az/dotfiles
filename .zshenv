# basic environment variables
export EDITOR="nvim"
export TERMINAL="kitty"
export PAGER="less"
export WM="awesome"
export PATH="$HOME/.local/bin:$PATH"
export HISTFILE=~/.zsh_history
export HISTSIZE=1000 # number of commands loaded into the memory from the history file
export SAVEHIST=1000 # number of commands stored in the history file

# qt5ct allows for configuring look and feel of qt5 application
export QT_QPA_PLATFORMTHEME="qt5ct"

# nnn 
export NNN_FIFO="/tmp/nnn.fifo"
export NNN_PLUG="o:fzopen;p:preview-tui;d:dragdrop"
export NNN_BMS="c:~/.config;e:/run/media"

# Start blinking
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
# Start bold
export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
# Start stand out
export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
# End standout
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
# Start underline
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red
# End Underline
export LESS_TERMCAP_ue=$(tput sgr0)
# End bold, blinking, standout, underline
export LESS_TERMCAP_me=$(tput sgr0)
